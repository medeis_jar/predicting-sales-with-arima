
# coding: utf-8

# In[10]:


# scipy
import scipy
print('scipy: %s' % scipy.__version__)
# numpy
import numpy
print('numpy: %s' % numpy.__version__)
# matplotlib
import matplotlib
print('matplotlib: %s' % matplotlib.__version__)
# pandas
import pandas
print('pandas: %s' % pandas.__version__)
# scikit-learn
import sklearn
print('sklearn: %s' % sklearn.__version__)
# statsmodels
import statsmodels
print('statsmodels: %s' % statsmodels.__version__)


get_ipython().run_line_magic('matplotlib', 'inline')
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 15, 6  


# In[3]:


# Wczytanie danych z pliku csv z uwzględnieniem kolumny z datą/czasem
data = pandas.read_csv('/home/pawelglica/Documents/magisterka/sales_org.csv',sep=';', parse_dates=['CLOSEDATETIME'], index_col='CLOSEDATETIME')


# In[3]:


# Wyswietl nagłowek danych
data.head()


# In[4]:


data.info()


# In[63]:


# Wczytanie danych z pliku csv z uwzględnieniem kolumny z datą/czasem
data = pandas.read_csv('/home/pawelglica/Documents/magisterka/sales_org.csv',sep=';', parse_dates=['CLOSEDATETIME'], index_col='CLOSEDATETIME')

# Przygotowanie danych
ts = data['CHECKTOTAL']
all = ts['2016-09-01':'2018-02-28'].resample('D').sum()
train = ts['2016-09-01':'2017-08-31'].resample('D').sum()
test = ts['2017-09-01':'2018-02-28'].resample('D').sum()

print(test)
all.describe()


# In[6]:



from sklearn.metrics import mean_squared_error
from math import sqrt
# Dane testowe
# Naiwne prognozowanie
history = [x for x in train]
predictions = list()
for i in range(len(test)):
	# prognozowanie
	yhat = history[-1]
	predictions.append(yhat)
	# obserwacje
	obs = test[i]
	history.append(obs)
	print('>Prognoza=%.3f, Oczekiwanie=%3.f' % (yhat, obs))
# raport wydajności
mse = mean_squared_error(test, predictions)
rmse = sqrt(mse)
print('RMSE: %.3f' % rmse)


# In[5]:


train.plot()
test.plot()


# In[46]:





# In[6]:


# Gestość obserwacji
from matplotlib import pyplot
pyplot.figure(1)
pyplot.subplot(211)
train.hist()
pyplot.subplot(212)
train.plot(kind='kde')


# In[28]:


from pandas import Series
from pandas.core import datetools
from statsmodels.tsa.stattools import adfuller
from matplotlib import pyplot
 
# tworzenie zroznicowanego szeregu czasowego
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return Series(diff)
 
series = train
X = series.values
X = X.astype('float32')
# różnicowanie danych
seasonal_interval = 7
stationary = difference(X, seasonal_interval)
stationary.index = series.index[seasonal_interval:]
# sprawdzanie stacjonarności
result = adfuller(X)
print('ADF Statistic: %f' % result[0])
print('p-value: %f' % result[1])
print('Critical Values:')
for key, value in result[4].items():
	print('\t%s: %.3f' % (key, value))
# zapisujemy dane, przydadzą się później
stationary.to_csv('stationary.csv')
# rysowanie
stationary.plot()


# In[29]:


from pandas import Series
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
from matplotlib import pyplot
series = Series.from_csv('stationary.csv')

pyplot.figure()
pyplot.subplot(211)
plot_acf(series, ax=pyplot.gca(), lags=50)
pyplot.subplot(212)
plot_pacf(series, ax=pyplot.gca(), lags=50)
pyplot.show()


# In[ ]:


import warnings
from pandas import Series
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error

# oecena moelu ARIMA na baze parametrów (p,d,q)
def evaluate_arima_model(X, arima_order):
	history = [x for x in train]
	# prognozuj
	predictions = list()
	for t in range(len(test)):
		model = ARIMA(history, order=arima_order)
		model_fit = model.fit(disp=0)
		yhat = model_fit.forecast()[0]
		predictions.append(yhat)
		history.append(test[t])
	# obliczanie mse
	error = mean_squared_error(test, predictions)
	return error

# ocenianie parametrów p,d,q w dmodelu ARIMA
def evaluate_models(dataset, p_values, d_values, q_values):
	dataset = dataset.astype('float32')
	best_score, best_cfg = float("inf"), None
	for p in p_values:
		for d in d_values:
			for q in q_values:
				order = (p,d,q)
				try:
					mse = evaluate_arima_model(dataset, order)
					if mse < best_score:
						best_score, best_cfg = mse, order
					print('ARIMA%s MSE=%.3f' % (order,sqrt(mse))
				except:
					continue
	print('Best ARIMA%s RMSE=%.3f' % (best_cfg, sqrt(best_score))

# wybrany szereg czasowy
series = all
# zbiory parametrów do oceny
p_values = [1, 2, 6, 7, 8, 14]
d_values = [0]
q_values = [1, 2, 4, 5, 6, 7]
warnings.filterwarnings("ignore")
evaluate_models(series, p_values, d_values, q_values)


# In[34]:


from pandas import Series
from sklearn.metrics import mean_squared_error
from statsmodels.tsa.arima_model import ARIMA
from math import sqrt

# zrożnicowane serie
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return diff

# odwracanie różnicowania
def inverse_difference(history, yhat, interval=1):
	return yhat + history[-interval]

# naiwna prognoza
history = [x for x in train]
predictions = list()
for i in range(len(test)):
	# róznicowanie danych
	seasonal_interval = 7
	diff = difference(history, seasonal_interval)
	# prognozowanie
	model = ARIMA(diff, order=(14,0,4))
	model_fit = model.fit(trend='nc', disp=0)
	yhat = model_fit.forecast()[0]
	yhat = inverse_difference(history, yhat, seasonal_interval)
	predictions.append(yhat)
	# obserwacje
	obs = test[i]
	history.append(obs)
	print('>Prognoza=%.3f, Oczekiwanie=%3.f' % (yhat, obs))
# wynik jakosci 
mse = mean_squared_error(test, predictions)
rmse = sqrt(mse)
print('RMSE: %.3f' % rmse)


# In[14]:





# In[35]:


from pandas import Series
from pandas import DataFrame
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot

# zrożnicowane serie
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return diff

# odwracanie różnicowania
def inverse_difference(history, yhat, interval=1):
	return yhat + history[-interval]

# naiwna prognoza
history = [x for x in train]
predictions = list()
for i in range(len(test)):
	# róznicowanie danych
	seasonal_interval = 7
	diff = difference(history, seasonal_interval)
	# prognozowanie
	model = ARIMA(diff, order=(14,0,4))
	model_fit = model.fit(trend='nc', disp=0)
	yhat = model_fit.forecast()[0]
	yhat = inverse_difference(history, yhat, seasonal_interval)
	predictions.append(yhat)
	# obserwacje
	obs = test[i]
	history.append(obs)
# błędy
residuals = [test[i]-predictions[i] for i in range(len(test))]
residuals = DataFrame(residuals)
print(residuals.describe())
# wykres
pyplot.figure()
pyplot.subplot(211)
residuals.hist(ax=pyplot.gca())
pyplot.subplot(212)
residuals.plot(kind='kde', ax=pyplot.gca())
pyplot.show()


# In[36]:


from pandas import Series
from pandas import DataFrame
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot

# zrożnicowane serie
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return diff

# odwracanie różnicowania
def inverse_difference(history, yhat, interval=1):
	return yhat + history[-interval]

# naiwna prognoza
history = [x for x in train]
predictions = list()
bias = 60.793873
for i in range(len(test)):
	# róznicowanie danych
	seasonal_interval = 7
	diff = difference(history, seasonal_interval)
	# prognozowanie
	model = ARIMA(diff, order=(14,0,4))
	model_fit = model.fit(trend='nc', disp=0)
	yhat = model_fit.forecast()[0]
	yhat = bias + inverse_difference(history, yhat, seasonal_interval)
	predictions.append(yhat)
	# obserwacje
	obs = test[i]
	history.append(obs)
# błędy
residuals = [test[i]-predictions[i] for i in range(len(test))]
residuals = DataFrame(residuals)
print(residuals.describe())
# wykres
pyplot.figure()
pyplot.subplot(211)
residuals.hist(ax=pyplot.gca())
pyplot.subplot(212)
residuals.plot(kind='kde', ax=pyplot.gca())
pyplot.show()


# In[62]:


from pandas import Series
from pandas import DataFrame
from statsmodels.tsa.arima_model import ARIMA
from matplotlib import pyplot
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf

# zrożnicowane serie
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return diff

# odwracanie różnicowania
def inverse_difference(history, yhat, interval=1):
	return yhat + history[-interval]

# naiwna prognoza
history = [x for x in train]
predictions = list()
for i in range(len(test)):
	# różnicowanie danych
	seasonal_interval = 7
	diff = difference(history, seasonal_interval)
	# prognozowanie
	model = ARIMA(diff, order=(14,0,4))
	model_fit = model.fit(trend='nc', disp=0)
	yhat = model_fit.forecast()[0]
	yhat = inverse_difference(history, yhat, seasonal_interval)
	predictions.append(yhat)
	# obserwacje
	obs = test[i]
	history.append(obs)
# błędy
residuals = [test[i]-predictions[i] for i in range(len(test))]
residuals = DataFrame(residuals)
print(residuals.describe())
# wykres
pyplot.figure()
pyplot.subplot(211)
plot_acf(residuals, ax=pyplot.gca())
pyplot.subplot(212)
plot_pacf(residuals, ax=pyplot.gca())
pyplot.show()


# In[61]:


# finalizacja

from pandas import Series
from statsmodels.tsa.arima_model import ARIMA
from scipy.stats import boxcox
import numpy
import time
import datetime
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
print("Przed",st)

# poprawa błedów przy zapisie i ładowaniu modelu - bug
def __getnewargs__(self):
	return ((self.endog),(self.k_lags, self.k_diff, self.k_ma))

ARIMA.__getnewargs__ = __getnewargs__

# zrożnicowane serie
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return diff

# wybranie szeregu czasowego
series = all
# przygotowanie danych
X = series.values
X = X.astype('float32')
# difference data
seasonal_interval = 7
diff = difference(X, seasonal_interval)
# dopasowanie modelu
model = ARIMA(diff, order=(14,0,4))
model_fit = model.fit(trend='nc', disp=0)
# poprawka, stała wyliczona z błedu losowego średniej
bias = 60.793873
# zapisanie modelu i poprawki 
model_fit.save('model.pkl')
numpy.save('model_bias.npy', [bias])
ts2 = time.time()
st2 = datetime.datetime.fromtimestamp(ts2).strftime('%Y-%m-%d %H:%M:%S')
print("Po",st2) 


# In[64]:


# prognozowanie

from pandas import Series
from statsmodels.tsa.arima_model import ARIMAResults
import numpy
import time
import datetime
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
print("Przed",st)
 
# odwracanie różnicowania
def inverse_difference(history, yhat, interval=1):
	return yhat + history[-interval]
 
series = test
seasonal_interval = 7
model_fit = ARIMAResults.load('model.pkl')
bias = numpy.load('model_bias.npy')
yhat = float(model_fit.forecast()[0])
yhat = bias + inverse_difference(series.values, yhat, seasonal_interval)
print('Prognoza: %.3f' % yhat)
ts2 = time.time()
st2 = datetime.datetime.fromtimestamp(ts2).strftime('%Y-%m-%d %H:%M:%S')
print("Po",st2) 


# In[45]:


from pandas import Series
from matplotlib import pyplot
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.arima_model import ARIMAResults
from sklearn.metrics import mean_squared_error
from math import sqrt
import numpy

# zrożnicowane serie
def difference(dataset, interval=1):
	diff = list()
	for i in range(interval, len(dataset)):
		value = dataset[i] - dataset[i - interval]
		diff.append(value)
	return diff

# odwracanie różnicowania
def inverse_difference(history, yhat, interval=1):
	return yhat + history[-interval]

# ładowanie i przygotowanie szeregów danych
dataset = all
X = dataset.values.astype('float32')
history = [x for x in X]
seasonal_interval = 7
validation = test
y = validation.values.astype('float32')
# ładowanie modelu
model_fit = ARIMAResults.load('model.pkl')
bias = numpy.load('model_bias.npy')
# pierwsza prognoza
predictions = list()
yhat = float(model_fit.forecast()[0])
yhat = bias + inverse_difference(history, yhat, seasonal_interval)
predictions.append(yhat)
history.append(y[0])
print('>Prognoza=%.3f, Oczekiwanie=%3.f' % (yhat, y[0]))
# kolejne prognozy
for i in range(1, len(y)):
	# rózncowanie danych
	seasonal_interval = 7
	diff = difference(history, seasonal_interval)
	# prognozowanie
	model = ARIMA(diff, order=(14,0,4))
	model_fit = model.fit(trend='nc', disp=0)
	yhat = model_fit.forecast()[0]
	yhat = bias + inverse_difference(history, yhat, seasonal_interval)
	predictions.append(yhat)
	# obserwacje
	obs = y[i]
	history.append(obs)
	print('>Prognoza=%.3f, Oczekiwanie=%3.f' % (yhat, obs))
# wynik jakości modelu
mse = mean_squared_error(y, predictions)
rmse = sqrt(mse)
print('RMSE: %.3f' % rmse)
pyplot.plot(y)
pyplot.plot(predictions, color='red')
pyplot.show()

